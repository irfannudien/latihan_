// Opsi Pertama
var { keyAdd, keyMul } = require("./import.js");
const os = require("os");

// Opsi Kedua
// var math = require("./import.js");
// let keyAdd = math.keyAdd;
// let keyMul = math.keyMul;

console.log(`Free memory: ${os.freemem()}`);
console.log(keyAdd(1, 4));
console.log(keyMul(1, 4));
