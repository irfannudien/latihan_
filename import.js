function add(a, b) {
  return a + b;
}

// Multiply
function mul(a, b) {
  return a * b;
}

// Module.Export harus 1x
module.exports = {
  keyAdd: add,
  keyMul: mul,
};
