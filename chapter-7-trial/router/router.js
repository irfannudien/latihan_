const router = require("express").Router();
const auth = require("../controllers/authController");
const restrict = require("../middlewares/restrict");

router.get("/api/whoami", restrict, auth.whoami);

//home page
router.get("/", (req, res) => res.render("index"));
//register page
router.get("/api/register", (req, res) => res.render("register"));
router.post("/api/register", auth.register);

//login page
router.get("/api/login", (req, res, next) => res.render("login"));
router.post("/api/login", auth.login);

module.exports = router;

// const router = require("express").Router();

// // Controllers
// const auth = require("../controllers/authController");
// // const restrict = require("../middlewares/restrict");

// // router.get("/api/whoami", restrict, auth.whoami);

// // Homepage
// router.get("/", (req, res) => res.render("index"));
// // Register Page
// router.get("/api/register", (req, res) => res.render("register"));
// router.post("/api/register", auth.register);
// // // Login Page
// router.get("/api/login", (req, res) => res.render("login"));
// router.post("/api/login", auth.login);

// module.exports = router;
