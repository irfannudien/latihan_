const { urlencoded } = require("express");
const express = require("express");
const app = express();
const router = require("./router/router");
const passport = require("./lib/passport");
const { PORT = 4000 } = process.env;

app.use(passport.initialize());

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const db = require("./models/index");
const NEVER_CHANGE = { force: false };
db.sequelize.sync(NEVER_CHANGE); // if true: overwrite the data table every NPM RUN START, false: not overwrite!!

app.set("view engine", "ejs");

app.use(router);
app.listen(PORT, () => {
  console.log(`Server nyala di port ${PORT}`);
});
