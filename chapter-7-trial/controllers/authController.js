const { User } = require("../models");

module.exports = {
  register: (req, res, next) => {
    // Kita panggil static method register yang sudah kita buat tadi
    User.register(req.body)
      .then((cek) => {
        if (cek) {
          res.redirect("/api/login");
        } else {
          res.send({ message: "something wrong" });
        }
      })
      .catch((err) => next(err));
  },
  login: (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        const { id, username } = user;
        const data = {
          id,
          username,
          token: user.generateToken(),
        };
        res.send(data);
      })
      .catch((err) => {
        res.send({ message: err.message });
      });
  },

  whoami: (req, res) => {
    const currentUser = req.user;
    res.json(currentUser);
  },
  // whoami: (req, res) => {
  //   const currentUser = req.user;
  //   const { id, username } = currentUser;
  //   res.send({ id, username });
  // },
};
